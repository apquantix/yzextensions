//
//  NSDictionary+YZ.swift
//  YZExtensions
//
//  Created by Bonifatio Hartono on Y17M04D22.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

public enum NSDictionaryConversionErr : Error {
	case StringConversionError(requestKey: String, from: NSDictionary)
	case IntegerConversionError(requestKey: String, from: NSDictionary)
	case FloatConversionError(requestKey: String, from: NSDictionary)
}

extension NSDictionary {

	public func letsUnwrapStringValueForKey(_ key: String) throws -> NSString {
		guard let aStr = self[key] as? NSString else {
			throw NSDictionaryConversionErr.StringConversionError(requestKey: key, from: self)
		}
		return aStr
	}

	public func letsUnwrapStringIntegerValueForKey(_ key: String) throws -> NSNumber{
		do {
			return try self.letsUnwrapStringValueForKey(key).integerValue as NSNumber
		}
		catch {
			throw error
		}
	}

	public func letsUnwrapStringFloatValueForKey(_ key: String) throws -> NSNumber{
		do {
			return try self.letsUnwrapStringValueForKey(key).floatValue as NSNumber
		}
		catch {
			throw error
		}
	}

	public func letsUnwrapArrayOfDictionaryForKey(_ key: String) throws -> [NSDictionary] {
		guard let arrDict = self[key] as? [NSDictionary] else {
			throw NSDictionaryConversionErr.StringConversionError(requestKey: key, from: self)
		}
		return arrDict
	}

}
