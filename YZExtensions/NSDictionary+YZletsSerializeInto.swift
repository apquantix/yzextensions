//
//  NSDictionary+YZletsSerializeInto.swift
//  YZExtensions
//
//  Created by Bonifatio Hartono on Y17M05D02.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

/// Error during serialization
///
/// - RequiredPropertyNotFound: Required Property not found
/// - TypeMismatch: Expecting different type
enum SerializeErr : Error {
	case RequiredPropertyNotFound(String)
	case TypeMismatch(key: String, expected: Any.Type)
}

public extension NSDictionary {



	//TODO: Handling enum properties.
	/// Dictionary into YZSafelyREST objects
	///
	/// - Parameter obj: destination object
	/// - Returns: produce object
	/// - Throws: NSDictionary.SerializeErr
	public func letsSerializeInto<T: YZSafelyRESTBaseModel>(obj: T) throws -> T {
		let m = Mirror(reflecting: obj)

		/// Iterates through obj properties.
		for prop in m.children {

			/// Capture the property type.
			let propType = type(of: prop.value)
			if let propKey = prop.label {

				/* Required Property Handler */

				if propType == ImplicitlyUnwrappedOptional<NSString>.self {

					guard let sVal = self[propKey] else {
						throw SerializeErr.RequiredPropertyNotFound(propKey)
					}
					guard let dVal = sVal as? NSString else {
						throw SerializeErr.TypeMismatch(key: propKey, expected: propType)
					}
					obj.setValue(dVal, forKey: propKey)

				}

				else if propType == ImplicitlyUnwrappedOptional<NSNumber>.self {

					guard let sVal = self[propKey] else {
						throw SerializeErr.RequiredPropertyNotFound(propKey)
					}
					if let dVal = sVal as? NSString {
						let fVal = dVal.floatValue
						obj.setValue(fVal, forKey: propKey)
					}
					else if let dVal = sVal as? NSNumber {
						obj.setValue(dVal, forKey: propKey)
					}
					else {
						throw SerializeErr.TypeMismatch(key: propKey, expected: propType)
					}
				}
				else if propType == Optional<NSString>.self {
					/// TODO: Work on optional String.
				}
				else {
					// Check for composition types.

					if let childTypeArr = obj.compositionTypes() {
						for childType in childTypeArr {
							if propType == childType {
								if let aChildDict = self[propKey] as? NSDictionary {
									do {
										try obj.produceCollaboration(key: propKey, val: aChildDict)
									}
									catch {
										throw error
									}
								}
							}
						} // end of childTypeArr iteration.
					}
					// No composition here.
				}
			}
			else {
				/// Something goes wrong here.. can't get property label.
			}
		}
		return obj
	}
	
}

