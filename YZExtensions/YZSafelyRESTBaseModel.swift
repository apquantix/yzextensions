//
//  YZSafelyRESTBaseModel.swift
//  YZExtensions
//
//  Created by Bonifatio Hartono on Y17M05D02.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

//FIXME: put more effort on changing this to protocol instead
/// Base Model
public class YZSafelyRESTBaseModel : NSObject {
	/// when new keys found during morph .. this will be filled in
	var newKeys : [String] = []


	/// override requires
	/// returning new model object by request defined as key
	///
	/// - Parameters:
	///   - key: property key that is requested
	///   - val: data value of a dictionary in the key section
	/// - Throws: NSDictionary.SerializeErr
	public func produceCollaboration(key: String, val: NSDictionary) throws {
	}

	/// override requires.
	/// Returning the composition types that is in use by the model
	///
	/// - Returns: <#return value description#>
	public func compositionTypes() -> [Any.Type]? {
		return nil
	}
}
