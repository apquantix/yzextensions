//
//  URLComponents+YZ.swift
//  YZExtensions
//
//  Created by Bonifatio Hartono on Y17M04D24.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

enum URLComponentsErr : Error {
	case EmptySchemeErr
	case InvalidScheme
}
public extension URLComponents {

	/// URLComponents init that also prevent the invalid exception on the scheme.
	///
	/// - Parameters:
	///   - scheme: URI scheme ( RFC 2396 )
	init(scheme: String) throws{
		self.init()

		// check scheme if it's allowable characters
		if scheme.isEmpty {
			throw URLComponentsErr.EmptySchemeErr
		}
		if scheme.range(of: "^[a-zA-Z][0-9a-zA-Z\\-\\.]*$", options: .regularExpression) != nil{
			throw URLComponentsErr.InvalidScheme
		}

		self.scheme = scheme
	}
	init(scheme: String, host: String) throws{
		do {
			try self.init(scheme: scheme)
			self.host = host
		}
		catch {
			throw error
		}
	}

	init(scheme: String, machineName: String, domain: String) throws {
		do {
			try self.init(scheme: scheme, host: machineName + "." + domain)
		}
		catch{
			throw error
		}
	}
}
