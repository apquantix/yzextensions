//
//  NSMutableURLRequest+YZ.swift
//  YZExtensions
//
//  Created by Bonifatio Hartono on Y17M03D14.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

public extension NSMutableURLRequest {

	convenience init(withBearer: String){
		self.init()
		self.addValue("Bearer \(withBearer)", forHTTPHeaderField: "Authorization")
	}

	convenience init(withBearer: String, url: URL?){
		self.init(withBearer: withBearer)
		self.url = url
	}
}

