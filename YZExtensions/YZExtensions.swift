//
//  YZExtensions.swift
//  YZExtensions
//
//  Created by Bonifatio Hartono on Y17M03D22.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

/// TODO: need a way to retain value type
func assign(_ inAny : Any? , orThrow: Error) throws -> Any {
	guard let result = inAny else{
		throw orThrow
	}
	return result
}
