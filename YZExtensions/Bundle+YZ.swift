//
//  Bundle+YZ.swift
//  YZExtensions
//
//  Created by Bonifatio Hartono on Y17M03D22.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

extension Bundle {

	enum IDVersionError : Error {
		case BundleDictionaryNotFound
		case BundleIdentifierNotFound
		case BundleShortVersionStringNotFound
	}


	/// Compose service name for keychain access usage
	///
	/// - Returns: service name
	/// - Throws: IDVersionError
	public func letsComposeServiceName() throws -> String {

		guard let bundleDictionary = self.infoDictionary else { throw IDVersionError.BundleDictionaryNotFound }
		guard let bundleIdentifier = self.bundleIdentifier else { throw IDVersionError.BundleIdentifierNotFound }
		guard let bundleShortVersion = bundleDictionary["CFBundleShortVersionString"] else { throw IDVersionError.BundleShortVersionStringNotFound }
		return "\(bundleIdentifier) (Ver. \(bundleShortVersion))"
	}
}
