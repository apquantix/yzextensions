//
//  NSURLRequest+YZ.swift
//  YZExtensions
//
//  Created by Bonifatio Hartono on Y17M03D17.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

public extension URLRequest {

	enum YZSendRequestError : Error {

		// FIXME: need better way to do this.
		/// thrown when http status code is not 200, 201, 202
		case HTTPResponseNotOK(URLResponse)
		/// thrown when the data found empty.
		case EmptyDataError

		/* not sure if being used */
			case DataTaskError(Error)
			case EndPointError
			case MessageTypeError
			case SerializationError
			case ValidationError
		/* */
	}

	/// Deals data and error also HTTP response
	public func letsSendRequest(response : @escaping (Data?, Error?) ->  () ) {

		// moving from mutable to immutable
		let aRequest : URLRequest = self

		// Create NSURLSession to manage the connection to the endpoint
		let aSession: URLSession = URLSession.shared

		// Create data task with request that we constructed
		let aDataTask = aSession.dataTask(with: aRequest as URLRequest) {
			(inData, inResponse, inError) -> Void in

			if let anHTTPResponse = inResponse as? HTTPURLResponse {
				if	anHTTPResponse.statusCode != 200 &&
					anHTTPResponse.statusCode != 201 &&
					anHTTPResponse.statusCode != 202 {
					response(nil, YZSendRequestError.HTTPResponseNotOK(anHTTPResponse))
					return
				}
			}

			// If there is no error ( inError == nil ) then go on.. otherwise execute the else clause
			guard inError == nil else{
				response(nil, inError)
				return
			}
			guard inData != nil else{
				response(nil, YZSendRequestError.EmptyDataError)
				return
			}

			response(inData, nil)
		}

		/* actually perform the connection to the endpoint */
		aDataTask.resume()
		
	}

	public func letsSendJSONRequest(response: @escaping (NSDictionary?, NSArray?, Error?) -> () ) {

		self.letsSendRequest { (inData, inError) in

			guard inError == nil else {
				response(nil, nil, inError)
				return
			}

			do {
				let resultData = try JSONSerialization.jsonObject(with: inData!, options: JSONSerialization.ReadingOptions.mutableContainers)
				// decide to return array or dictionary
				if let anArray: NSArray = resultData as? NSArray {
					response(nil, anArray, nil)
					return
				}
				if let aDictionary: NSDictionary = resultData as? NSDictionary {
					response(aDictionary, nil, nil)
					return
				}


			}
			catch {
				response(nil, nil, error)
				return
			}

		}
	}
}
