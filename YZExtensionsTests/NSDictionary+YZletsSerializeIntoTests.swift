//
//  NSDictionary+YZletsSerializeIntoTests.swift
//  YZExtensions
//
//  Created by Bonifatio Hartono on Y17M05D02.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import XCTest

class Sample : YZSafelyRESTBaseModel{
	var x : NSString!
	var y : NSString!
}

class NSDictionary_YZletsSerializeIntoTests: XCTestCase {



	override func setUp() {
		super.setUp()
		// Put setup code here. This method is called before the invocation of each test method in the class.
	}

	override func tearDown() {
		// Put teardown code here. This method is called after the invocation of each test method in the class.
		super.tearDown()
	}



	func testSimple() {



		let dict : NSDictionary = [
			"x" : "XXX",
			"y" : "YYY"
		]

		do {
			let x = try dict.letsSerializeInto(obj: Sample())
			XCTAssertNotNil(x.x)
			XCTAssertEqual(x.x, "XXX")
		}
		catch {
			print(error)
		}


	}
}
