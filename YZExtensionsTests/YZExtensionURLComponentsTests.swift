//
//  YZExtensionURLComponentsTests.swift
//  YZExtensions
//
//  Created by Bonifatio Hartono on Y17M04D24.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import XCTest

class YZExtensionURLComponentsTests: XCTestCase {

	func testInitialization(){
		do {
			let c1 = try URLComponents(scheme: "https", host: "example.com")
			XCTAssert(c1.url?.absoluteString == "https://example.com")

			let c2 = try URLComponents(scheme: "https23", host: "example.com")
			XCTAssert(c2.url?.absoluteString == "https23://example.com")
		}
		catch {
			print(error)
		}

	}
}
